# G1 monetary license

## Available languages
- [(`en`) English](g1_monetary_license_en.rst)
- [(`eo`) Esperanto](g1_monetary_license_eo.rst)
- [(`es`) Spanish](g1_monetary_license_es.rst)
- [(`fr`) French](g1_monetary_license_fr.rst)
- [(`pt`) Portuguese](g1_monetary_license_pt.rst)
- [(`it`) Italian](g1_monetary_license_it.rst)
- [(`de`) Deutsch](g1_monetary_license_de.rst)
- [(`ca`) Catalan](g1_monetary_license_ca.rst)
